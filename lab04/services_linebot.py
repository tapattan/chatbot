from flask import Flask, request, abort
app = Flask(__name__)
from linebot import (
LineBotApi, WebhookHandler
)
from linebot.exceptions import (
InvalidSignatureError
)
from linebot.models import (
MessageEvent, TextMessage, TextSendMessage,
)

from tvDatafeed import TvDatafeed, Interval

########## edit ##############
line_bot_api = LineBotApi('Channel access token')  #แก้เป็นคีย์ของคุณ
handler = WebhookHandler('Channel secret')

@app.route('/')
def index():
    return 'Hello World!'

@app.route('/webhook', methods=['GET','POST'])
def webhook():
    
    body = request.get_data(as_text=True)
    # print(body)
    req = request.get_json(silent=True, force=True)
    intent = req["queryResult"]["intent"]["displayName"]
    text = req['originalDetectIntentRequest']['payload']['data']['message']['text']
    reply_token = req['originalDetectIntentRequest']['payload']['data']['replyToken']
    id = req['originalDetectIntentRequest']['payload']['data']['source']['userId']
    disname = line_bot_api.get_profile(id).display_name
    
    value = req["queryResult"]["parameters"]["symbol"]
    print(req)
    print('id = ' + id)
    print('name = ' + disname)
    print('text = ' + text)
    print('intent = ' + intent)
    print('reply_token = ' + reply_token)
    print('value = '+value)
    
    tv = TvDatafeed()
    df = tv.get_hist(symbol=value,exchange='set',interval=Interval.in_1_minute,n_bars=1)
    close = df['close'][0]
    text = 'ราคา '+value+ ' ล่าสุดที่ '+str(close)
    reply(intent,text,reply_token,id,disname)

    return 'OK'


def reply(intent,text,reply_token,id,disname):
        text_message = TextSendMessage(text=text)
        line_bot_api.reply_message(reply_token,text_message)
     

@handler.add(MessageEvent, message=TextMessage)
def handle_message(event):
    line_bot_api.reply_message(
    event.reply_token,
    TextSendMessage(text=event.message.text))

if __name__ == '__main__':
    app.run(debug=True)