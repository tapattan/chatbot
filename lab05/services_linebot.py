from flask import Flask, request, abort , url_for
import matplotlib.pyplot as plt
from flask import Response
from matplotlib.figure import Figure
import pandas as pd 

import matplotlib
matplotlib.use('Agg')

app = Flask(__name__)
from linebot import (
LineBotApi, WebhookHandler
)
from linebot.exceptions import (
InvalidSignatureError
)
from linebot.models import (
MessageEvent, TextMessage, TextSendMessage,ImageSendMessage
)

from tvDatafeed import TvDatafeed, Interval

######### edit ##############
line_bot_api = LineBotApi('Channel access token')  #แก้เป็นคีย์ของคุณ
handler = WebhookHandler('Channel secret')


@app.route('/')
def index():
    return 'Hello World!'

@app.route('/webhook', methods=['GET','POST'])
def webhook():
    
    body = request.get_data(as_text=True)
    # print(body)
    req = request.get_json(silent=True, force=True)

    print(req)

    intent = req["queryResult"]["intent"]["displayName"]
    text = req['originalDetectIntentRequest']['payload']['data']['message']['text']
    reply_token = req['originalDetectIntentRequest']['payload']['data']['replyToken']
    id = req['originalDetectIntentRequest']['payload']['data']['source']['userId']
    disname = line_bot_api.get_profile(id).display_name
    
    value = req["queryResult"]["parameters"]["symbol"]
    
    print('id = ' + id)
    print('name = ' + disname)
    print('text = ' + text)
    print('intent = ' + intent)
    print('reply_token = ' + reply_token)
    print('value = '+value)
    
    if(intent=='ขอราคา'):
        tv = TvDatafeed()
        df = tv.get_hist(symbol=value,exchange='set',interval=Interval.in_1_minute,n_bars=1)
        close = df['close'][0]
        text = 'ราคา '+value+ ' ล่าสุดที่ '+str(close)
        reply(intent,text,reply_token,id,disname)
    
    elif(intent=='ขอกราฟ'):

        tv = TvDatafeed()
        df = tv.get_hist(symbol=value,exchange='set',interval=Interval.in_daily,n_bars=100)
        close = df[['close']]

        fig = Figure()
        axis = fig.add_subplot(1, 1, 1)
        plt.plot(close.index,close['close'])
        plt.title(value)
        plt.xlabel('date')
        plt.ylabel('bath')
        plt.savefig('static/'+value+'.png')
        plt.cla()
        plt.clf()
        plt.close()
        imgname = value
        replyimg(reply_token,imgname+'.png')

    return 'OK'

 

def replyimg(reply_token,imgname):
    img_address = url_for('static', filename=imgname)
    print(img_address)
    img_address = 'https://2ea3-49-228-235-224.ap.ngrok.io'+img_address
    print(img_address)

    image_message = ImageSendMessage(
    original_content_url=img_address,
    preview_image_url=img_address)
    line_bot_api.reply_message(reply_token,image_message)

def reply(intent,text,reply_token,id,disname):
        text_message = TextSendMessage(text=text)
        line_bot_api.reply_message(reply_token,text_message)
     

@handler.add(MessageEvent, message=TextMessage)
def handle_message(event):
    line_bot_api.reply_message(
    event.reply_token,
    TextSendMessage(text=event.message.text))

if __name__ == '__main__':
    app.run(debug=True)